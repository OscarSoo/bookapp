<html> 
<head>
    <link href="view/css/bootstrap.min.css" rel="stylesheet">
    <link href="view/css/grayscale.css" rel="stylesheet">
    <link href="view/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    
</head>  
   
<body id="page-top" data-spy="scroll" data-target=".navbar-fixed-top">  
<!-- Navigation -->
    <nav class="navbar navbar-custom navbar-fixed-top" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-main-collapse">
                    <i class="fa fa-bars"></i>
                </button>
                <a class="navbar-brand page-scroll" href="#page-top">
                    <span class="light">书店</span>BookStore
                </a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse navbar-right navbar-main-collapse">
                <ul class="nav navbar-nav">
                    <!-- Hidden li included to remove active class from about link when scrolled up past about section -->
                    <li class="hidden">
                        <a href="#page-top"></a>
                    </li>
                    <li>
                        <a class="page-scroll" href="/bookapp/admin/">Admin</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="#catalog">View Books</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>
    
    <!-- Intro Header -->
    <header class="intro">
        <div class="intro-body">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        <h1 class="brand-heading">书店</span>BookStore</h1>
                        <p class="intro-text">Welcome</p>
                        <a href="#catalog" class="btn btn-circle page-scroll">
                            <i class="fa fa-angle-double-down animated"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </header>
    

<!-- Cataglog-->
<div id="catalog" align="center" class="container content-section">
    <div class="table-responsive"> 
    <table class="table ">  
        <tbody>
            <tr>
                <td></td>
                <td>Title</td>
                <td>Author</td>
                <td>Description</td>
            </tr>
        </tbody>  
        
        <?php   
        //echo var_dump($books);
            for ($i=0; $i <= count($books) -1; $i++)  
            {  
                echo '
                <tr>
                    <td class="btn btn-default btn-lg">
                    <a href="index.php?book='.$books[$i]['title'].'">
                    <img src="admin/upload/'.$books[$i]['thumbnail'].'" height="=250" width="150"/>
                    </td>
                    <td>  
                    <a href="index.php?book='.$books[$i]['title'].'">'
                    .$books[$i]['title'].'</a>
                    </td>
                    <td class="col-md-2">'
                    .$books[$i]['author'].'
                    </td>
                    <td>'
                    .$books[$i]['description'].'
                    </td>
                </tr>';  
            }  
        ?>  
    </table>
</div>
</div>
</div>
    
     <!-- Testing Purposes -->
    <section class="container content-section text-center">
    </section>
        
<!-- Footer -->
    <footer>
        <div class="container text-center">
            <p>书店 Copyright &copy; 2017</p>
        </div>
    </footer>    

<!-- jQuery -->
<script src="view/js/jquery.js"></script>

 <!-- Bootstrap Core JavaScript -->
<script src="view/js/bootstrap.min.js"></script>

<!-- Plugin JavaScript -->
<script src="view/js/jquery.easing.min.js"></script>   
    
<!-- Custom Theme JavaScript -->
<script src="view/js/grayscale.js"></script>
        
  
</body>  
</html>  