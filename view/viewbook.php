<html> 
<head>
    <link href="view/css/bootstrap.min.css" rel="stylesheet">
    <link href="view/css/grayscale.css" rel="stylesheet">
    <link href="view/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
<style>
 .background-image {
  position: fixed;
  left: 0;
  right: 0;
  z-index: 1;

  display: block;
  background-image: url('view/img/intro-bg.jpg');
  width: 100%;
  height: 1800%;

  -webkit-filter: blur(5px);
  -moz-filter: blur(5px);
  -o-filter: blur(5px);
  -ms-filter: blur(5px);
  filter: blur(5px);
}

.content {
  position: fixed;
  left: 0;
  right: 0;
  z-index: 9999;
  margin-left: 20px;
  margin-right: 20px;
}
</style>
</head>  

<div class="background-image"></div>
    <div class="content">
<body id="page-top" data-spy="scroll" data-target=".navbar-fixed-top">  
<!-- Navigation -->
    <nav class="navbar navbar-custom navbar-fixed-top" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-main-collapse">
                    <i class="fa fa-bars"></i>
                </button>
                <a class="navbar-brand page-scroll" href="index.php">
                    <span class="light">书店</span>BookStore
                </a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse navbar-right navbar-main-collapse">
                <ul class="nav navbar-nav">
                    <!-- Hidden li included to remove active class from about link when scrolled up past about section -->
                    <li class="hidden">
                        <a href="index.php"></a>
                    </li>
                    <li>
                        <a  href="/bookapp/admin/">Admin</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="index.php">View Books</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
            
            <img scr="view/img/intro-bg.jpg">
        </div>
        <!-- /.container -->
    </nav> 
    


<!-- Display Selected Book Name -->   
    
    <div align="center" class="container content-section">
    <div class="table-responsive"> 
        
    <table class="table">  
        <tbody>
            <tr>
                <td></td>
                <td>Title</td>
                <td>Author</td>
                <td>Description</td>
                <td>Email</td>
            </tr>
        </tbody>  
        
    <?php 
    
    for ($i=0; $i <= count($books) -1; $i++)  
            {  
                echo '
                <tr>
                    <td class="btn btn-default btn-lg col-md-1">
                    <img src="admin/upload/'.$books[$i]['thumbnail'].'" height="=250" width="150"/>
                    </td>
                    <td class="col-md-2">'
                    .$books[$i]['title'].'</a>
                    </td>
                    <td class="col-md-1">'
                    .$books[$i]['author'].'
                    </td>
                    <td class="col-md-6">'
                    .$books[$i]['description'].'
                    </td>
                    <td class="col-md-2">'
                    .$books[$i]['email'].'
                    </td>
                </tr>';  
            } 
    ?>
</table>
    </div>
</div>
        
    
 <!-- Footer -->
<footer>
        <div class="container text-center">
            <p>书店 Copyright &copy; 2017</p>
        </div>
</footer>  
        


<!-- jQuery -->
<script src="view/js/jquery.js"></script>

 <!-- Bootstrap Core JavaScript -->
<script src="view/js/bootstrap.min.js"></script>

<!-- Plugin JavaScript -->
<script src="view/js/jquery.easing.min.js"></script>   
    
<!-- Custom Theme JavaScript -->
<script src="view/js/grayscale.js"></script>

</body>  
</html>  


 
