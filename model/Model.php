<?php
include("config.php");

class Model 
{  
    public function getBookList()  
    {   
        global $conn;
        
        $result = $conn->query("SELECT * from book");
        $return_result = array();
        
        if ($result->num_rows > 0) 
                {				
                    while($row = $result->fetch_assoc())
                    {  
                        //return var_dump($row);
                        $return_result[] = array( 
                             'title' => $row["title"],
                             'author' => $row["author"],
                             'email' => $row["email"],
                             'thumbnail' => $row["thumbnail"],
                             'description' => $row["description"]
                            );
                    }    
                return $return_result;
                }
                else 
                {	
                    return "No Record Found!<br><br>";
                }  
    }  
      
    public function getBook($title)  
    {    

        global $conn;
        
        $result = $conn->query("SELECT * FROM book WHERE title = '$title'");
        $return_result = array();
        
        if ($result->num_rows > 0) 
                {				
                    while($row = $result->fetch_assoc())
                    {  
                        //return var_dump($row);
                        $return_result[] = array( 
                             'title' => $row["title"],
                             'author' => $row["author"],
                             'email' => $row["email"],
                             'thumbnail' => $row["thumbnail"],
                             'description' => $row["description"]
                            );
                    }    
                return $return_result;
                }
                else 
                {	
                    return "No Record Found!<br><br>";
                }
        
       // $allBooks = $this->getBookList();  
       // return $allBooks[$title];  
    }  
      
      
}  
?>