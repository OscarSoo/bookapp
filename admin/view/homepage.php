<!DOCTYPE html>
<html lang="en">

<head>

    <title>BookStore Admin</title>

    <!-- Bootstrap Core CSS -->
    <link href="view/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="view/css/simple-sidebar.css" rel="stylesheet">
    
    <!-- Inline Confirmation For Delete -->
    <script src="http://code.jquery.com/jquery-1.11.1.min.js"></script>
    <script language="JavaScript" type="text/javascript">
    $(document).ready(function(){
        $("a.delete").click(function(e){
            if(!confirm('Delete Confirm?')){
                e.preventDefault();
                return false;
            }
            return true;
        });
    });
    </script>

</head>

<body>

    <div id="wrapper">

        <!-- Sidebar -->
        <div id="sidebar-wrapper">
            <ul class="sidebar-nav">
                <li class="sidebar-brand">
                    <a href="../index.php">
                        书店 BookStore
                    </a>
                </li>
                <li>
                    <a href='index.php?value=add'>Add Book</a>
                </li>
                <li>
                    <a href="#wrapper">View Book</a>
                </li>

                <li>
                    <a href='index.php?logout=logout'>Logout</a>
                </li>
            </ul>
        </div>
        <!-- /#sidebar-wrapper -->

        <!-- Page Content -->
        <div id="page-content-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <a href="#menu-toggle" class="btn btn-info btn-sm btn-warning" id="menu-toggle">Menu</a>
                        
                        <h1>书店 Admin</h1>
                        
<div class="table-responsive"> 
    <table class="table ">  
        <tbody>
            <tr>
                <td></td>
                <td>Title</td>
                <td>Author</td>
                <td>Description</td>
                <td>Email</td>
                <td></td>
                <td></td>
            </tr>
        </tbody>  
        
        <?php   
        //echo var_dump($books);
            for ($i=0; $i <= count($books) -1; $i++)  
            {  
                echo '
                <tr>
                    <td class="btn btn-default btn-lg">
                   
                    <img src="upload/'.$books[$i]['thumbnail'].'" height="=250" width="150"/>
                    </td>
                    <td class="col-md-2">'
                    .$books[$i]['title'].'
                    </td>
                    <td class="col-md-2">'
                    .$books[$i]['author'].'
                    </td>
                    <td>'
                    .$books[$i]['description'].'
                    </td>
                    <td>'
                    .$books[$i]['email'].'
                    </td>
                    <td>
                    <a href="index.php?value='.$books[$i]['id'].'" class=" btn btn-warning">Edit</a>
                    </td>
                    <td>
                    <a href="index.php?delete='.$books[$i]['id'].'" class=" btn btn-warning delete" value>Delete</a>
                    </td>                  
                </tr>';  
            }  
        ?>  
    </table>
</div>                    

                </div>
            </div>
        </div>
        <!-- /#page-content-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="view/css/js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="view/css/js/bootstrap.min.js"></script>

    <!-- Menu Toggle Script -->
    <script>
    $("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
    });      
    </script>

</body>

</html>

