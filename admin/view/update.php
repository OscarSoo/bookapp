<!DOCTYPE html>
<html lang="en">

<head>

    <title>BookStore Admin</title>

    <!-- Bootstrap Core CSS -->
    <link href="view/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="view/css/simple-sidebar.css" rel="stylesheet">


</head>

<body>

    <div id="wrapper">

        <!-- Sidebar -->
        <div id="sidebar-wrapper">
            <ul class="sidebar-nav">
                <li class="sidebar-brand">
                    <a href="../index.php">
                        书店 BookStore
                    </a>
                </li>
                <li>
                    <a href="index.php?value=add">Add Book</a>
                </li>
                <li>
                    <a href="index.php">View Book</a>
                </li>

                <li>
                    <a href='index.php?logout=logout'>Logout</a>
                </li>
            </ul>
        </div>
        <!-- /#sidebar-wrapper -->

        <!-- Page Content -->
        <div id="page-content-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <a href="#menu-toggle" class="btn btn-info btn-sm btn-warning" id="menu-toggle">Menu</a>
                      
                        
                        <h1>Update Book</h1>
                        

            <div class="table-responsive"> 
                <form method = "POST" enctype="multipart/form-data">
                    <table class="table" >
                       <?php   
                        // echo var_dump($books);
                            for ($i=0; $i <= count($books) -1; $i++)  
                            {  
                                $id = $books[$i]['id'];
                                $thumbnail = $books[$i]['thumbnail'];
                                $title = $books[$i]['title'];
                                $author = $books[$i]['author'];
                                $description = $books[$i]['description'];
                                $email = $books[$i]['email'];    
                            } 
                        ?> 
                        <tbody>                       
                            <tr>                            
                                <td width="30%"><img src="upload/<?php echo htmlspecialchars($thumbnail)?>" height="=500" width="250"/></td>
                                <td>
                                Select image to upload:<br><input type="file" name="thumbnail">                         
                                <input type="hidden" name="id" value="<?php echo htmlspecialchars($id)?>" required><br><br>                      
                                Title:<br><input type="text" name="title" value="<?php echo htmlspecialchars($title)?>" required><br><br>
                                Author:<br><input type="text" name="author" value="<?php echo htmlspecialchars($author)?>" required><br><br>
                                Email:<br><input type="text" name="email" value="<?php echo htmlspecialchars($email)?>" required pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$"><br><br>
                                Description:<br><input type="text" name="description" value="<?php echo htmlspecialchars($description)?>" required><br><br>
                                <input class="btn btn-info btn-sm btn-warning" type="submit" name="update" value="Update"><br>
                                </td>
                            </tr>
                     </table>
                </form>
            </div>                    

                </div>
            </div>
        </div>
        <!-- /#page-content-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="view/css/js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="view/css/js/bootstrap.min.js"></script>
        
    <script src="view/css/js/update.js"></script>
        

    <!-- Menu Toggle Script -->
    <script>
    $("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
    });
    </script>

</body>
</html>
