<?php
include_once("model/Model.php");
session_start();
class Controller {
 public $model;
 
public function __construct()  
    {  
        $this->model = new Model();
    } 
 
public function invoke()
{

    
    if(!$_SESSION)
    {
    switch ($_REQUEST['task'])
        {
            case "login":

                 if(isset($_REQUEST['submit']))
                {

                    if (empty($_REQUEST['username']) || empty($_REQUEST['password'])) 
                    {
                        echo  "Invalid Username or Password";
                    }
                    else
                    {
                        $result = $this->model->getlogin(($_REQUEST['username']),($_REQUEST['password']));
                        if($result)
                        {       //var_dump ($_SESSION);
                                $this->display();                            
                        }
                        else
                        {
                            echo "Invalid user or password";
                        }
                    }
                }

            break;

            default:
                include 'view/login.php';
            break;
        }

    }
    else if (isset($_SESSION))
    {
        $this->logout();
        $this->delete();
        $this->display();     
       
    }
 }
  
public function display()  
     {   
    
          if (!isset($_GET['value']))  
          {  
               $books = $this->model->getBookList();  
               include 'view/homepage.php'; 
          } 
          else 
          {       
                switch ($_GET['value'])
                {
                    case 'add':
                    include 'view/add.php';
                    $this->add();
                    break;
                    
                    default:                        
                    $books = $this->model->getBook($_GET['value']); 
                    include 'view/update.php'; 
                    $this->update();   
                    break;                      
                } 
          }  
     } 
    
public function logout()
    {
        if (isset($_GET['logout']))
          {            
            session_unset();
            session_destroy();
            header ('Location: index.php');
          }
    }
 

public function add() 
{
        $this->model->add(($_REQUEST['id']),($_REQUEST['title']),($_REQUEST['author']),($_REQUEST['description']),($_REQUEST['email']));
} 
    
public function update()
{
    if (isset($_REQUEST['update']))
    {
        $this->model->update(($_REQUEST['id']),($_REQUEST['thumbnail']),($_REQUEST['title']),($_REQUEST['author']),($_REQUEST['description']),($_REQUEST['email']));
        header( "Refresh: 0;" );
    }           
}
    
public function delete() 
{   
    if (isset($_REQUEST['delete']))
    {   
        $this->model->delete(($_REQUEST['delete']));
        header ('Location: index.php');
    }
    
} 

    
}
    
?>