<?php
include("config.php");
include_once("controller/Controller.php");

$file = $_FILES['thumbnail'];
$name = $file['name'];


class Model {

 public function getlogin($username,$password)
 {
    
    global $conn;
     
    $username = $_POST['username'];
    $password = $_POST['password'];
     

            $sql="SELECT * FROM admin WHERE user='$username' AND password ='$password'";
            $res=mysqli_query($conn,$sql);

            if($res->num_rows > 0 )
                {
                    $_SESSION['login_user']=$username;
                return true;
                }
            else
                {
                return false;
                }
    
}

//Show BookList function
public function getBookList()  
    {   
        global $conn;
        
        $result = $conn->query("SELECT * from book");
        $return_result = array();
        
        if ($result->num_rows > 0) 
                {				
                    while($row = $result->fetch_assoc())
                    {  
                        //return var_dump($row);
                        $return_result[] = array( 
                            'id' => $row['id'],
                             'title' => $row["title"],
                             'author' => $row["author"],
                             'email' => $row["email"],
                             'thumbnail' => $row["thumbnail"],
                             'description' => $row["description"]
                            );
                    }    
                return $return_result;
                }
                else 
                {	
                return "No Record Found!<br><br>";
                }  
    }

//Get specific book function
public function getBook($title)
{
    global $conn;
        
        $result = $conn->query("SELECT * FROM book WHERE `id` = '$title'");
        $return_result = array();
        
        if ($result->num_rows > 0) 
                {				
                    while($row = $result->fetch_assoc())
                    {  
                        //return var_dump($row);
                        $return_result[] = array( 
                            'id' => $row["id"],
                             'title' => $row["title"],
                             'author' => $row["author"],
                             'email' => $row["email"],
                             'thumbnail' => $row["thumbnail"],
                             'description' => $row["description"]
                            );
                    }    
                return $return_result;
                }
                else 
                {	
                    return "No Record Found!<br><br>";
                }
}

//Add book function
public function add($id, $title, $author, $description, $email)
{
    global $conn;

        global $name;
        

        //If input is not empty then run the query
        if(empty ($title))
	    {
            echo "No Entry.</br>";
	    }
        else
        {
            $thumbnail = $name;
            $this->upload();
            
            $result = $conn->query("INSERT INTO BOOK (title, author, email, description, thumbnail) VALUE('$title','$author','$email', '$description', '$name')");
            
            if ($result === TRUE) 
			{
				echo "<script type='text/javascript'>alert('Record Added Successfully!')</script>";  
			} 
			else 
			{
				echo "</br></br>Error: " . $query . "<br>" . $conn->error;
            } 
        }
}

//Update books
public function update($id, $thumbnail, $title, $author, $description, $email)
{
    global $conn;
  
    
    $check = $conn->query("SELECT id FROM book WHERE id  = '$id'");

    if($check != true)
	{
		echo "No Book Found.</br>";
	}
    else 
    {

        global $name;
        $thumbnail = $name;
        $this->upload();
        
        $query = "";
        if (!empty($title))
        {
                $query .= " title ='$title' ";
        }
        if (!empty($author))
        {
                $query .= " ,author ='$author' ";
        }
        if (!empty($email))
        {
                $query .= " ,email ='$email' ";
        }
        if (!empty($description))
        {
                $query .= " ,description ='$description' ";
        } 
        if (!empty($thumbnail))
        {
                $query .= " ,thumbnail ='$name' ";
        }
        
        $result = $conn->query("UPDATE book SET $query WHERE id = '$id'");
        
        if ($result === TRUE) 
			{
				echo "<script type='text/javascript'>alert('Record Updated Successfully!')</script>";  
			} 
			else 
			{
				echo "</br></br>Error: " . $query . "<br>" . $conn->error;
            } 
    }
}

//Upload Picture Function
public function upload()
{
        global $file;
        global $name;
    
        if (!empty($name))
        {
            $path = "upload/" . basename($name);
            if (move_uploaded_file($file['tmp_name'], $path)) 
            {
                $thumbnail = $name;
            }
        }     
}

//Delete Function
public function delete($id)
{
        global $conn;
    
        $check = $conn->query("SELECT id FROM book WHERE id  = '$id'");

        if($check != true)
        {
            echo "No Book Found.</br>";
        }
        else 
        {

            $result = $conn->query("DELETE FROM book WHERE id = '$id'");
        
            if ($result === TRUE) 
			{
				echo "<script type='text/javascript'>alert('Record Deleted Successfully!')</script>"; 
			} 
			else 
			{
				echo "</br></br>Error: " . $query . "<br>" . $conn->error;
            }  
        }       
}
    
}
?>
